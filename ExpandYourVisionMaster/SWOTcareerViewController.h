//
//  SWOTcareerViewController.h
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWOTcareerViewController : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}
- (IBAction)hidekeyboard:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *Question;
@property (weak, nonatomic) IBOutlet UITextField *userinput;

@property (strong,nonatomic) NSString *strength;
@property (strong,nonatomic) NSString *function;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
