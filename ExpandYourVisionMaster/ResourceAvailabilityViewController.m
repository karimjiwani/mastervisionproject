//
//  ResourceAvailability1ViewController.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "ResourceAvailabilityViewController.h"
#import "ResourceAvailabilityWhatIsStoppingYouViewController.h"
#import "AddViewController.h"

@interface ResourceAvailabilityViewController ()

@end

@implementation ResourceAvailabilityViewController
@synthesize CareerName = _CareerName;
@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Continue:(id)sender {
    self.Career = self.CareerName.text;
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   /*if ( [[segue identifier] isEqualToString:kRAtoAddOption] ) {
        ResourceAvailabilityWhatIsStoppingYouViewController *ResourceAvailabilityWhatIsStoppingYouViewController = segue.destinationViewController;
        ResourceAvailabilityWhatIsStoppingYouViewController.Idealcareer = self.CareerName.text;
    }*/
    
    if([[segue identifier] isEqualToString:kRAtoAddOption]){
        AddViewController *addViewController = segue.destinationViewController;
        addViewController.selectedMethod = kResourceAvailability;
        addViewController.selectedOptionDC = self.CareerName.text;
        addViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
    
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:kRAtoAddOption] ) {
        if ( [self.CareerName.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Career name should not be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}


- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
}

- (IBAction)backToMethods: (UIStoryboardSegue *)segue{
    
}
@end
