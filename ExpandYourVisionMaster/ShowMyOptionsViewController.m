//
//  ShowMyOptionsViewController.m
//  ExpandYourVisionMaster
//
//  Created by Karim Jiwani on 2014-02-14.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "ShowMyOptionsViewController.h"
#import "OptionList.h"
#import "DecisionCriteria.h"

@interface ShowMyOptionsViewController ()

@end

@implementation ShowMyOptionsViewController {
    NSArray *optionsList;
    NSArray *desicionCriteriaList;
    NSNumber *userNumber;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize optionsTableView = _optionsTableView, decisionCriteriaTableView = _decisionCriteriaTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.optionsTableView.delegate = self;
    self.decisionCriteriaTableView.delegate = self;
    userNumber = [NSNumber numberWithInt:[[NSUserDefaults standardUserDefaults] integerForKey:@"userCount"]];
    [self fetchDecisionCriteriaLists];
    [self fetchOptionsList];
    [self.optionsTableView reloadData];
    [self.decisionCriteriaTableView reloadData];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fetchDecisionCriteriaLists {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DecisionCriteria" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSExpression *leftExpression = [NSExpression expressionForKeyPath:@"userID"];
    NSExpression *rightExpression = [NSExpression expressionForConstantValue:userNumber];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:leftExpression rightExpression:rightExpression modifier:NSDirectPredicateModifier type:NSContainsPredicateOperatorType options:NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch];
    [fetchRequest setPredicate:predicate];
    desicionCriteriaList = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSLog(@"%lu", desicionCriteriaList.count);
}

- (void) fetchOptionsList {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"OptionList" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSExpression *leftExpression = [NSExpression expressionForKeyPath:@"userID"];
    NSExpression *rightExpression = [NSExpression expressionForConstantValue:userNumber];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:leftExpression rightExpression:rightExpression modifier:NSDirectPredicateModifier type:NSContainsPredicateOperatorType options:NSDiacriticInsensitiveSearch|NSCaseInsensitiveSearch];
    [fetchRequest setPredicate:predicate];
    optionsList = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSLog(@"%lu", optionsList.count);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( tableView == self.optionsTableView ) {
        return optionsList.count;
    } else {
        return desicionCriteriaList.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell;
    NSLog(@"%ld", (long)[tableView numberOfRowsInSection:1]);
    if ( tableView == self.optionsTableView ) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"OptionCell"];
        if ( cell == nil ) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OptionCell"];
        }
        OptionList *option = [optionsList objectAtIndex:indexPath.row];
        cell.textLabel.text = option.optionName;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DecisionCell"];
        if ( cell == nil ) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DecisionCell"];
        }
        DecisionCriteria *dc = [desicionCriteriaList objectAtIndex:indexPath.row];
        
        cell.textLabel.text = dc.decisionName;
    }
    return cell;
}


@end
