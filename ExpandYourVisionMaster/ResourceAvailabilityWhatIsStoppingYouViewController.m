 //
//  ResourceAvailabilityWhatIsStoppingYouViewController.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "ResourceAvailabilityWhatIsStoppingYouViewController.h"
#import "AddViewController.h"

@interface ResourceAvailabilityWhatIsStoppingYouViewController ()

@end


@implementation ResourceAvailabilityWhatIsStoppingYouViewController
@synthesize StoppingConstraintTextField = _StoppingConstraintTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSString *question = @"What is stopping you from adding ";
    NSString *questionPart2 = @"to your Option List?";
    question = [question stringByAppendingFormat:@"%@ %@", self.Idealcareer, questionPart2];
    self.WhatisStoppingyouQuestion.text = question;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
}

- (IBAction)Continue:(id)sender {
    self.StoppingConstraint = self.StoppingConstraintTextField.text;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:kRA2toAddOption]){
        AddViewController *addViewController = segue.destinationViewController;
        addViewController.selectedMethod = kResourceAvailability;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}
@end
