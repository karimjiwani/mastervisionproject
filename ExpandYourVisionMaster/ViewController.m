//
//  ViewController.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "ViewController.h"
#import "UserInfo.h"
#import "MenuViewController.h"
#import "Industry.h"

@interface ViewController ()

@end

@implementation ViewController {
    NSInteger userCount;
}

@synthesize managedObjectContext = _managedObjectContext;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)jumpToHomeScreen:(UIStoryboardSegue *)segue {
    
}
- (void) updateUserCount {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    userCount = [userDefaults integerForKey:@"userCount"];
    if ( !userCount ) {
        userCount = 1;
        
    } else {
        userCount = userCount + 1;
    }
    [userDefaults setInteger:userCount forKey:@"userCount"];
    [userDefaults synchronize];
}

- (IBAction)newUserButtonPressed:(UIButton *)sender {
    [self updateUserCount];
    [self resetIndustryFlags];
    NSLog(@"%d", userCount);
    UserInfo *userInfo = [NSEntityDescription insertNewObjectForEntityForName:@"UserInfo" inManagedObjectContext:self.managedObjectContext];
    userInfo.userID = [NSNumber numberWithInt:userCount];
    userInfo.returningUser = [NSNumber numberWithBool:NO];
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error Saving UserInfo: %@", error );
    }
}
- (IBAction)retruningUserButtonPressed:(UIButton *)sender {
    [self updateUserCount];
    [self resetIndustryFlags];
    NSLog(@"Testing: %d", userCount); //Comments
    UserInfo *userInfo = [NSEntityDescription insertNewObjectForEntityForName:@"UserInfo" inManagedObjectContext:self.managedObjectContext];
    userInfo.userID = [NSNumber numberWithInt:userCount];
    userInfo.returningUser = [NSNumber numberWithBool:YES];
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error Saving UserInfo: %@", error );
        
    }
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    MenuViewController *menuViewController = segue.destinationViewController;
    menuViewController.managedObjectContext = self.managedObjectContext;
}

- (void) resetIndustryFlags {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Industry" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    NSExpression *leftExpression = [NSExpression expressionForKeyPath:@"flag"];
    NSExpression *rightExpression = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:1]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:leftExpression rightExpression:rightExpression modifier:NSDirectPredicateModifier type:NSContainsPredicateOperatorType options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch];
    [fetchRequest setPredicate:predicate];
    NSArray *industries = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    for ( Industry *industry in industries ) {
        [industry setFlag:[NSNumber numberWithBool:NO]];
    }
    [self.managedObjectContext save:nil];
}

@end
