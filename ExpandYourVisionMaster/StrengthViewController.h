//
//  StrengthViewController.h
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrengthViewController : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}
- (IBAction)hidekeyboard:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userinput;
@property (strong,nonatomic) NSString *LongtermG;
@property (strong, nonatomic) NSString *ShorttermG;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UILabel *strengthQuestion;

- (IBAction)anotherStrength:(UIStoryboardSegue *)segue;
@end
