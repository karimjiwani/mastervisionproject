//
//  LTGViewController.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "LTGViewController.h"
#import "STGViewController.h"
#import "StrengthViewController.h"

@interface LTGViewController ()

@end

@implementation LTGViewController
@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"LTtoST"] ) {
        STGViewController *stgViewController = segue.destinationViewController;
        stgViewController.LongtermG = self.userinput.text;
        stgViewController.managedObjectContext = self.managedObjectContext;
    
    } else if ( [[segue identifier] isEqualToString:@"LTtoStrength"] ) {
        StrengthViewController *strengthViewController = segue.destinationViewController;
        strengthViewController.LongtermG = self.userinput.text;
        strengthViewController.managedObjectContext = self.managedObjectContext;
        
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:@"LTtoStrength"] ) {
        if ( [self.userinput.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please input your long term goal to continue." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        } else {
            return YES;
        }
    }
   
    return YES;
}
- (IBAction)backtomenu:(UIStoryboardSegue *)segue {
    
}

- (IBAction)backToMethods: (UIStoryboardSegue *)segue {
    
}


@end
