//
//  RoleStorming1ViewController.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoleStorming1ViewController : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}
- (IBAction)hideheyboard:(id)sender;



@property (strong, nonatomic) IBOutlet UITextField *RoleModelNameTextField;
@property (strong, nonatomic) NSString *RoleModelName;
@property (strong, nonatomic) IBOutlet UILabel *ThinkOfaRM;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)Continue:(UIButton *)sender;
- (IBAction)backToRoleStroming1:(UIStoryboardSegue *)segue;

- (IBAction)backToMethods: (UIStoryboardSegue *)segue;


@end
