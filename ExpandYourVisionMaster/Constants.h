//
//  Constants.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/10/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#ifndef ExpandYourVisionMaster_Constants_h
#define ExpandYourVisionMaster_Constants_h


#endif

#define kRoleStorming @"Role Storming"
#define kRStoAddOption @"RStoAddOption"
#define kResourceAvailability @"Resource Availability"
#define kRAtoAddOption @"RAtoAddOption"
#define kRA2toAddOption @"RA2toAddOption"
#define kTimeTravelSWOT @"Time Travel & SWOT"
#define kTTStoAddOption @"TTStoAddOption"
#define kRandomInput @"Random Input"
#define kRItoAddOption @"RItoAddOption"
#define kDecisionCriteria @"Decision Criteria"
#define kOption @"Option"
#define kYesFromAddpage @"YesFromAddPage"
#define kNoFromAddpage @"NoFromAddPage"
#define kRandomInputSelectionYes @"RandomInputSelectionYes"
#define kRandomInputSelectionNo @"RandomInputSelectionNo"
#define kshowMyOptions @"showMyOptions"
