//
//  ShowMyOptionsViewController.h
//  ExpandYourVisionMaster
//
//  Created by Karim Jiwani on 2014-02-14.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowMyOptionsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITableView *optionsTableView;
@property (weak, nonatomic) IBOutlet UITableView *decisionCriteriaTableView;

@end
