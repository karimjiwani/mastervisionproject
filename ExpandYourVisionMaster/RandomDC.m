//
//  RandomDC.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-01-29.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "RandomDC.h"

@interface RandomDC ()

@end

@implementation RandomDC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
