//
//  ResourceAvailability1ViewController.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResourceAvailabilityViewController : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}
- (IBAction)hidekeyboard:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *CareerName;
@property (strong, nonatomic) NSString *Career;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)Continue:(id)sender;

- (IBAction)backToMethods: (UIStoryboardSegue *)segue;

@end
