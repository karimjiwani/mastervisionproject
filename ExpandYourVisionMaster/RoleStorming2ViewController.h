//
//  RoleStorming2ViewController.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoleStorming2ViewController : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}

- (IBAction)hidekeyboard:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *RMSuggestionTextfield;
@property (weak, nonatomic) IBOutlet UILabel *suggestionQuestionLabel;

@property (strong, nonatomic) NSString *roleModelString;
@property (strong, nonatomic) NSString *RMSuggestion;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)Continue:(id)sender;
- (IBAction)ThinkOfAnotherRoleModel:(id)sender;


@end
