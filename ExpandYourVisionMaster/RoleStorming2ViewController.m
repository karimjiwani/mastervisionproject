//
//  RoleStorming2ViewController.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "RoleStorming2ViewController.h"
#import "AddViewController.h"

@interface RoleStorming2ViewController ()

@end

@implementation RoleStorming2ViewController {
    
}

@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ( self.roleModelString != nil  ) {
        [self initQuestionLabel];
    }
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Continue:(id)sender {
    self.RMSuggestion = self.RMSuggestionTextfield.text;
    NSLog(@"Option is %@",self.RMSuggestion);
}

- (IBAction)ThinkOfAnotherRoleModel:(id)sender {
    //if user can't think of an option, we take them back to the screen for thinking of a new role model.
}

- (void)initQuestionLabel {
    NSString *question = @"What is an Option that ";
    NSString *questionPart2 = @"would ask you to consider for your career?";
    question = [question stringByAppendingFormat:@"%@ %@", self.roleModelString, questionPart2];
    self.suggestionQuestionLabel.text = question;
    
}
- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;

    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:kRStoAddOption]){
        AddViewController *addViewController = segue.destinationViewController;
        addViewController.selectedMethod = kRoleStorming;
        addViewController.selectedOptionDC = self.RMSuggestionTextfield.text;
        addViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:kRStoAddOption]) {
        if ( [self.RMSuggestionTextfield.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Suggestion Field should not be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        }
    }
    return YES;
}
@end
