//
//  StrengthViewController.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "StrengthViewController.h"
#import "SWOTcareerViewController.h"

@interface StrengthViewController ()

@end

@implementation StrengthViewController
@synthesize managedObjectContext = _managedObjectContext, strengthQuestion = _strengthQuestion;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.strengthQuestion.text = @"Think of ONE past experience where you use a strength of yours. Write the strength below.";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"StrengthtoCareer"] ) {
        SWOTcareerViewController *swotVC = segue.destinationViewController;
        swotVC.strength = self.userinput.text;
        swotVC.managedObjectContext = self.managedObjectContext;
        ///
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:@"StrengthtoCareer"] ) {
        if ( [self.userinput.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please input your strength to continue." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        } else {
            return YES;
        }
    }
    
    return YES;
}

- (IBAction)anotherStrength:(UIStoryboardSegue *)segue {
    self.strengthQuestion.text = @"Think of ONE past experience where you use a DIFFERENT strength of yours. Write the new strength below.";
}

@end
