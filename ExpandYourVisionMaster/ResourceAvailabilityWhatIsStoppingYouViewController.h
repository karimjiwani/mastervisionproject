//
//  ResourceAvailabilityWhatIsStoppingYouViewController.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResourceAvailabilityWhatIsStoppingYouViewController : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}
- (IBAction)hidekeyboard:(id)sender;
- (IBAction)Continue:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *StoppingConstraintTextField;
@property (strong, nonatomic) NSString *StoppingConstraint;
@property (strong, nonatomic) NSString *Idealcareer;
@property (weak, nonatomic) IBOutlet UILabel *WhatisStoppingyouQuestion;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end
