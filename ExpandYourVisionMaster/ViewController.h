//
//  ViewController.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)jumpToHomeScreen:(UIStoryboard *)segue;
- (IBAction)newUserButtonPressed:(UIButton *)sender;
- (IBAction)retruningUserButtonPressed:(UIButton *)sender;


@end
