//
//  RoleStorming1ViewController.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "RoleStorming1ViewController.h"
#import "RoleStorming2ViewController.h"
#import "RandomDecisionCriteriaViewController.h"

@interface RoleStorming1ViewController ()

@end

@implementation RoleStorming1ViewController 
{

}
@synthesize RoleModelNameTextField = _RoleModelNameTextField;
@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.RoleModelNameTextField.delegate = self;
    self.ThinkOfaRM.text = @"Think of a Role Model and write their name below";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Continue:(UIButton *)sender
{
    self.RoleModelName = self.RoleModelNameTextField.text;
    NSLog(@"Role Model Name is %@",self.RoleModelName);
    
    //go to next screen
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"ToRoleStorming2"] ) {
        RoleStorming2ViewController *roleST2ViewController = segue.destinationViewController;
        roleST2ViewController.roleModelString = self.RoleModelNameTextField.text;
        roleST2ViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:@"ToRoleStorming2"] ) {
        if ( [self.RoleModelNameTextField.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Role Model name should not be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}

- (IBAction)backToRoleStroming1:(UIStoryboardSegue *)segue {
    self.ThinkOfaRM.text = @"Think of another Role Model and write their name below";
}

- (IBAction)hideheyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
    
}

- (IBAction)backToMethods: (UIStoryboardSegue *)segue {
    
}
@end
