//
//  STGViewController.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "STGViewController.h"
#import "StrengthViewController.h"

@interface STGViewController ()

@end

@implementation STGViewController
@synthesize managedObjectContext = _managedObjectContext;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"OnlyStrength"] ) {
        StrengthViewController *SViewController = segue.destinationViewController;
        ///
    }
    if (( [[segue identifier] isEqualToString:@"STandStrength"] )) {
        StrengthViewController *sViewController = segue.destinationViewController;
        sViewController.ShorttermG = self.userinput.text;
        sViewController.managedObjectContext = self.managedObjectContext;
        
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:@"STandStrength"] ) {
        if ( [self.userinput.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please input your short term goal to continue." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        } else {
            return YES;
        }
    }
    
    return YES;
}
@end
