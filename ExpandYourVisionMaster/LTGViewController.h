//
//  LTGViewController.h
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LTGViewController : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}
- (IBAction)hidekeyboard:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *userinput;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)backToMethods: (UIStoryboardSegue *)segue;

@end
