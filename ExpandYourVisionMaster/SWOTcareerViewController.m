//
//  SWOTcareerViewController.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "SWOTcareerViewController.h"
#import "AddViewController.h"

@interface SWOTcareerViewController ()

@end

@implementation SWOTcareerViewController
@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.function = @"LTG";//assign value for testing
    if(self.function == [NSString stringWithFormat:@"LTG"])
    {
        self.Question.text = [NSString stringWithFormat:@"Think of ONE career where you can apply your strength %@ and help you with the long term goal.",self.strength];
        
    }
    else if (self.function == [NSString stringWithFormat:@"STG"])
    {
        self.Question.text = [NSString stringWithFormat:@"Think of ONE career where you can apply your strength %@ and help you with the short term goal.",self.strength];
    }
    else
    {
        self.Question.text = [NSString stringWithFormat:@"Think of ONE career where you can apply your strength %@.",self.strength];
    }
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:kTTStoAddOption]){
        AddViewController *addViewController = segue.destinationViewController;
        addViewController.selectedMethod = kTimeTravelSWOT;
        addViewController.selectedOptionDC = self.userinput.text;
        addViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:kTTStoAddOption] ) {
        if ( [self.userinput.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Text Field should not be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}


@end
