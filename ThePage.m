//
//  ThePage.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "ThePage.h"

@interface ThePage ()

@end

@implementation ThePage
@synthesize selectedMethod = _selectedMethod, managedObjectContext = _managedObjectContext;
@synthesize listName = _listName;
@synthesize buttonPressed = _buttonPressed;

BOOL isContinueButtonPressed = NO;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userInput.delegate = self;
    //self.output = @"Professor";///assign values for testing
    	// Do any additional setup after loading the view.
    [self setEveryThingForPage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
}

- (void) setEveryThingForPage {
    self.method.text = self.selectedMethod;
    
    /*if( self.buttonPressed == NO && self.currentState == 0 && [self.selectedMethod isEqualToString:kResourceAvailability]) {
        self.Thinkof.text = [NSString stringWithFormat:@"Think of Career %@", self.selectedOptionDC];
        self.Question.text = [NSString stringWithFormat:@"What is stopping you from adding %@ to your Option List?", self.selectedOptionDC];
    } else if ( sel ) {
        
    } else {
        
    }*/
    
    if([self.listName isEqualToString:kDecisionCriteria])
        {
            self.Thinkof.text = [NSString stringWithFormat:@"Think of Decision Criterion %@",self.selectedOptionDC];
            if( self.buttonPressed ) {
                self.Question.text = [NSString stringWithFormat:@"Can you think of a career that is suited well for this criterion?"];
            } else {
                self.Question.text = [NSString stringWithFormat:@"Can you think of a career that is NOT suited well for this criterion?"];
            }
            
        }
        else
        {
            self.Thinkof.text = [NSString stringWithFormat:@"Think of Career %@", self.selectedOptionDC];
            if ( self.buttonPressed ) {
                    self.Question.text = [NSString stringWithFormat:@"Name ONE thing you like about this Career"];
            } else {
                self.Question.text = [NSString stringWithFormat:@"Name ONE thing you DO NOT like about this Career"];
            }
            // self.Question.text = [NSString stringWithFormat:@"Name ONE thing you like about this Career"];
        }
    

}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ( [self.userInput.text isEqualToString:@""] && isContinueButtonPressed ){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Text field should not be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        isContinueButtonPressed = NO;
        return NO;
        
    } else {
        
        return YES;
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (IBAction)areYouSure:(UIButton *)sender {
    
}

- (IBAction)continueButtonPress:(id)sender {
    isContinueButtonPressed = YES;
}
@end
