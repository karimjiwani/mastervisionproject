//
//  RandomDC.h
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-01-29.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RandomDecisionCriteriaViewController : UIViewController <UITextFieldDelegate,UIActionSheetDelegate>
{
    
    IBOutlet UITextField *hide;
}

@property (nonatomic) BOOL optionSelectedInRandomInput;
@property (strong,nonatomic) NSString *Att;
@property (strong,nonatomic) NSString *Industry;


///////industry from the Random VC//////
@property (strong,nonatomic) NSString *thinkIndustry;
@property (strong, nonatomic) IBOutlet UILabel *Question;
@property (strong, nonatomic) IBOutlet UITextField *potentialDC;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (weak, nonatomic) IBOutlet UILabel *Question2;//link to the second line of the question

- (IBAction)hidekeyboard:(id)sender;
- (IBAction)continue:(id)sender;

@end
