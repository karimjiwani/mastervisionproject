//
//  MenuViewController.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

- (IBAction)goBackToMenu:(UIStoryboardSegue *)segue;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
- (IBAction)reHelp:(id)sender;
- (IBAction)resourceAvailabilityInfo:(id)sender;
- (IBAction)roleStormingInfo:(id)sender;
- (IBAction)randomInput:(id)sender;
- (IBAction)timeTravelInfo:(id)sender;

@end
