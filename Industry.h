//
//  Industry.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Industry : NSManagedObject

@property (nonatomic, retain) NSNumber * flag;
@property (nonatomic, retain) NSNumber * industryID;
@property (nonatomic, retain) NSString * industryName;

@end
