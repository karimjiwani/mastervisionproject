//
//  Industry.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "Industry.h"


@implementation Industry

@dynamic flag;
@dynamic industryID;
@dynamic industryName;

@end
