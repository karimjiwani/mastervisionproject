//
//  MenuViewController.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "MenuViewController.h"
#import "RandomInputViewController.h"
#import "RoleStorming1ViewController.h"
#import "LTGViewController.h"
#import "ResourceAvailabilityViewController.h"
#import "ShowMyOptionsViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController {
    UIView *helpPopup;
}

@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if( self.managedObjectContext != nil ) {
        NSLog(@"managedObject");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBackToMenu:(UIStoryboardSegue *)segue{
    NSLog(@"In Menu Again!");
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if( [[segue identifier] isEqualToString:@"toRandomInput"] ) {
        RandomInputViewController *randomInputViewController = segue.destinationViewController;
        randomInputViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:@"toRoleStorming"]) {
        RoleStorming1ViewController *roleStormingViewController = segue.destinationViewController;
        roleStormingViewController.managedObjectContext = self.managedObjectContext;
        
    } else if ([[segue identifier] isEqualToString:@"toTimeTravel"]){
        LTGViewController *timeTravelViewController = segue.destinationViewController;
        timeTravelViewController.managedObjectContext = self.managedObjectContext;
        
    } else if ( [[segue identifier] isEqualToString:@"toResourceAvailability"]) {
        ResourceAvailabilityViewController *resourceViewController = segue.destinationViewController;
        resourceViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (IBAction)reHelp:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(initPopUpView)];
    
    helpPopup.alpha = 1;
    helpPopup.frame = CGRectMake(500, 500, 300, 400);
    
    [UIView commitAnimations];
}

- (IBAction)resourceAvailabilityInfo:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Resource Availability" message:@"Already have a dream job in your mind? Try Resource Availability to explore your possibilities." delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil];
    [alertView show];
}

- (IBAction)roleStormingInfo:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Role Storming" message:@"You may think differently when you are in another person’s shoe." delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil];
    [alertView show];
}

- (IBAction)randomInput:(id)sender {
   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Random Input" message:@"Random Input method triggers your thought process through random information shown." delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil];
    [alertView show];
}

- (IBAction)timeTravelInfo:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Time Travel" message:@"Picture yourself in 5 years? 10 years? Use this method aim your personal goal when looking far into the future." delegate:nil cancelButtonTitle:@"OK"  otherButtonTitles:nil];
    [alertView show];
}


- (void) initPopUpView {
    helpPopup.alpha = 0;
    helpPopup.frame = CGRectMake (500, 500, 0, 0);
    [self.view addSubview:helpPopup];
}
@end
