//
//  RandomInput.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-01-29.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "RandomInputViewController.h"
#import "RandomDecisionCriteriaViewController.h"
#import "MenuViewController.h"
#import "Industry.h"


@interface RandomInputViewController ()

@end

@implementation RandomInputViewController {
   // Industry *industry;
    NSNumber *index;
}
BOOL outOfIndustry = NO;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize noButton = _noButton;
@synthesize yesButton = _yesButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //index = [NSNumber numberWithInt:0];
    //[self checkflag];
    [self fetchIndustryAndUpdateFlag];
    if ( !outOfIndustry ) {
        self.Question.text = [NSString stringWithFormat:@"Are you interested in %@",self.industryName];
    }
    
}

-(int)getindustryID
{
    int IDnumber = arc4random()%24;
    return IDnumber;
}

-(NSUInteger) getIndustryID:(NSUInteger) totalCount {
    return arc4random() % totalCount;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)refreshIndustry:(id)sender {
    //go to next screen
    //index = [NSNumber numberWithInt:1];//index=1 means it is no like this industry

}

- (void) fetchIndustryAndUpdateFlag {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Industry" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    NSExpression *leftExpression = [NSExpression expressionForKeyPath:@"flag"];
    NSExpression *rightExpression = [NSExpression expressionForConstantValue:[NSNumber numberWithBool:NO]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:leftExpression rightExpression:rightExpression modifier:NSDirectPredicateModifier type:NSContainsPredicateOperatorType options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch];
    [fetchRequest setPredicate:predicate];
    NSArray *industries = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSUInteger industryCount = industries.count;
    if (industryCount > 0) {
        NSLog(@"Industry Count: %lu", industryCount);
        Industry *industry = [industries objectAtIndex:[self getIndustryID:industryCount]];
        self.industryName = industry.industryName;
        [industry setFlag:[NSNumber numberWithBool:YES]];
        [self.managedObjectContext save:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Out of Industries" message:@"Opps we ran out of industries option. Please select another method." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        self.noButton.enabled = NO;
        self.yesButton.enabled = NO;
       
        self.Question.text = @"Out of Industry";
        outOfIndustry = YES;
        
    }
    
}

-(void)checkflag
{
    NSFetchRequest *request;
    NSEntityDescription *entity;
    int industryID;
    NSArray *matchingdata;
    NSPredicate *predicate;
    NSError *error;
    
    while (matchingdata.count <=0)
    {
        industryID = [self getindustryID];
        request = [[NSFetchRequest alloc] init];
        entity = [NSEntityDescription entityForName:@"Industry" inManagedObjectContext:self.managedObjectContext];
        [request setEntity:entity];
        predicate = [NSPredicate predicateWithFormat:@"industryID==%@ && flag==%@", [NSNumber numberWithInt:industryID], [NSNumber numberWithInt:0]];
        [request setPredicate:predicate];
        matchingdata = [self.managedObjectContext executeFetchRequest:request error:&error];
    }
    
    for (Industry *obj in matchingdata) {
        NSLog(@" %@ %@", obj.industryID, obj.flag);
        obj.flag = [NSNumber numberWithInt:1];
        NSLog(@" %@ %@", obj.industryID, obj.flag);
        //[obj setValue: [NSNumber numberWithInt:1] forKey:@"flag"];
        //[obj setFlag:[NSNumber numberWithInt:1]];
        self.industryName = [obj valueForKey:@"industryName"];
        NSError *error2;
        if(![self.managedObjectContext save:& error2])
        {
            NSLog(@"Whoops, couldn't save: %@",[error2 localizedDescription]);
        }
    }

}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([[segue identifier] isEqualToString:kRandomInputSelectionYes]) {
        RandomDecisionCriteriaViewController *randomDecisionCriteriaViewController = segue.destinationViewController;
        randomDecisionCriteriaViewController.Industry = self.industryName;
        randomDecisionCriteriaViewController.optionSelectedInRandomInput = YES;
        randomDecisionCriteriaViewController.managedObjectContext = self.managedObjectContext;
    } else if([[segue identifier] isEqualToString:kRandomInputSelectionNo]) {
        RandomDecisionCriteriaViewController *randomDecisionCriteriaViewController = segue.destinationViewController;
        randomDecisionCriteriaViewController.Industry = self.industryName;
        randomDecisionCriteriaViewController.optionSelectedInRandomInput = NO;
        randomDecisionCriteriaViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (IBAction)backToMethods: (UIStoryboardSegue *)segue {
    [self fetchIndustryAndUpdateFlag];
    if ( !outOfIndustry) {
        self.Question.text = [NSString stringWithFormat:@"Are you interested in %@",self.industryName];
    }

    
}

- (IBAction)yesButtonPressed:(id)sender {
}

@end
