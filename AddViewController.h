//
//  AddViewController.h
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddViewController : UIViewController

@property (strong,nonatomic) NSString *MethodName;
//@property (strong,nonatomic) NSString *tagline;
@property (strong,nonatomic) NSString *function;
@property (strong,nonatomic) NSString *selectedOptionDC;
@property (strong,nonatomic) NSString *listName;
@property (strong,nonatomic) NSString *selectedMethod;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (weak, nonatomic) IBOutlet UILabel *Question;
@property (weak, nonatomic) IBOutlet UILabel *method;
//@property (weak, nonatomic) IBOutlet UILabel *tagline;

- (IBAction)backToAddPage:(UIStoryboardSegue *)segue;
- (IBAction)yesButtonPressed:(id)sender;


@end
