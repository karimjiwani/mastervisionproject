//
//  ThePage.h
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThePage : UIViewController <UITextFieldDelegate>
{
    
    IBOutlet UITextField *hide;
}
- (IBAction)hidekeyboard:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userInput;
@property (weak, nonatomic) IBOutlet UILabel *Thinkof;
@property (weak, nonatomic) IBOutlet UILabel *Question;
@property (weak, nonatomic) IBOutlet UILabel *method;
@property (weak, nonatomic) IBOutlet UILabel *tag;

@property (nonatomic) BOOL buttonPressed;
@property (strong,nonatomic) NSString *listName;
@property (strong,nonatomic) NSString *selectedOptionDC;
@property (strong, nonatomic) NSString *selectedMethod;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) NSInteger currentState;

- (IBAction)areYouSure:(UIButton *)sender;
- (IBAction)continueButtonPress:(id)sender;

@end
