//
//  UserInfo.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 2/2/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSNumber * returningUser;

@end
