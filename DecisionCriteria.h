//
//  DecisionCriteria.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DecisionCriteria : NSManagedObject

@property (nonatomic, retain) NSNumber * decisionID;
@property (nonatomic, retain) NSString * decisionName;
@property (nonatomic, retain) NSNumber * userID;

@end
