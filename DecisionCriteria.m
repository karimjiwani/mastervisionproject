//
//  DecisionCriteria.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "DecisionCriteria.h"


@implementation DecisionCriteria

@dynamic decisionID;
@dynamic decisionName;
@dynamic userID;

@end
