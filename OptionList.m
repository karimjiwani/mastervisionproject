//
//  OptionList.m
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "OptionList.h"


@implementation OptionList

@dynamic optionID;
@dynamic optionName;
@dynamic userID;

@end
