//
//  OptionList.h
//  ExpandYourVisionMaster
//
//  Created by Rehana Rajwani on 1/29/2014.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OptionList : NSManagedObject

@property (nonatomic, retain) NSNumber * optionID;
@property (nonatomic, retain) NSString * optionName;
@property (nonatomic, retain) NSNumber * userID;

@end
