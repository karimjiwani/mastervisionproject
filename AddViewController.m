//
//  AddViewController.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-02-02.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "AddViewController.h"
#import "ThePage.h"
#import "OptionList.h"
#import "DecisionCriteria.h"

@interface AddViewController ()

@end

@implementation AddViewController
@synthesize selectedMethod = _selectedMethod;
@synthesize method = _method;
@synthesize selectedOptionDC = _selectedOptionDC;
@synthesize managedObjectContext =_managedObjectContext;

int currentState = 0;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setEverythingOnThisPage];
    // Do any additional setup after loading the view.
}

- (void)setEverythingOnThisPage {
    self.method.text = self.selectedMethod;
    //self.MethodName = self.selectedMethod;
    if( currentState == 0){
        if ([self.selectedMethod isEqualToString:kRandomInput]){
            self.listName = kDecisionCriteria;
        } else {
            self.listName = kOption;
        }
    } else {
        if ( [self.listName isEqualToString:kDecisionCriteria] ) {
            self.listName = kOption;
        } else {
            self.listName = kDecisionCriteria;
        }
    }
    
    self.Question.text = [NSString stringWithFormat:@"Would you like to add %@ to your %@ list?",self.selectedOptionDC,self.listName];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"%@", [segue identifier]);
    if ( [segue identifier] != NULL ) {
        ThePage *thePage = segue.destinationViewController;
        thePage.selectedMethod = self.selectedMethod;
        thePage.listName = self.listName;
        thePage.selectedOptionDC = self.selectedOptionDC;
        thePage.currentState = currentState;
        thePage.managedObjectContext = self.managedObjectContext;
        if ( [segue.identifier isEqualToString:kYesFromAddpage] ) {
            thePage.buttonPressed = YES;
        } else {
            thePage.buttonPressed = NO;
        }
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
    
    
}

- (IBAction)backToAddPage:(UIStoryboardSegue *)segue {
    currentState++;
    ThePage *thePage = segue.sourceViewController;
    self.selectedOptionDC = thePage.userInput.text;
    [self setEverythingOnThisPage];
}

- (IBAction)yesButtonPressed:(id)sender {
    NSNumber *userCount = [NSNumber numberWithInt:[[NSUserDefaults standardUserDefaults] integerForKey:@"userCount"]];
    if ( [self.listName isEqualToString:kOption] ) {
        OptionList *option = [NSEntityDescription insertNewObjectForEntityForName:@"OptionList" inManagedObjectContext:self.managedObjectContext];
        option.optionName = self.selectedOptionDC;
        option.userID = userCount;
    } else {
        DecisionCriteria *decision = [NSEntityDescription insertNewObjectForEntityForName:@"DecisionCriteria" inManagedObjectContext:self.managedObjectContext];
        decision.userID = userCount;
        decision.decisionName = self.selectedOptionDC;
    }
    
    [self.managedObjectContext save:nil];
}
@end
