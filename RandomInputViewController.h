//
//  RandomInput.h
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-01-29.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RandomInputViewController : UIViewController

@property (strong,nonatomic) NSString *industryName;
@property (weak, nonatomic) IBOutlet UILabel *Question;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;

- (IBAction)refreshIndustry:(id)sender;
- (IBAction)backToMethods: (UIStoryboardSegue *)segue;


@end
