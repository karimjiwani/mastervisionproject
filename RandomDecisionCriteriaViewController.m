//
//  RandomDC.m
//  ExpandYourVisionMaster
//
//  Created by Yang Wu on 2014-01-29.
//  Copyright (c) 2014 Rehana Rajwani. All rights reserved.
//

#import "RandomDecisionCriteriaViewController.h"
#import "RandomInputViewController.h"
#import "AddViewController.h"

@interface RandomDecisionCriteriaViewController ()


@end

@implementation RandomDecisionCriteriaViewController
@synthesize managedObjectContext = _managedObjectContext;

@synthesize optionSelectedInRandomInput = _optionSelectedInRandomInput;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.potentialDC.delegate = self;
    self.Question.text = [NSString stringWithFormat:@"Think of %@",self.Industry];
    if(self.optionSelectedInRandomInput == YES)
    {
        self.Question2.text = [NSString stringWithFormat:@"Name ONE thing that you like about this industry."];
    }
    else
    {
        self.Question2.text = [NSString stringWithFormat:@"Name ONE thing that you do not like about this industry."];
    }
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hidekeyboard:(id)sender {
    UIView *dummy = [[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)];
    hide.inputView = dummy;
    
}
- (IBAction)continue:(id)sender {

}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:kRItoAddOption] ) {
        AddViewController *addViewController = segue.destinationViewController;
        addViewController.selectedMethod = kRandomInput;
        addViewController.selectedOptionDC = self.potentialDC.text;
        addViewController.managedObjectContext = self.managedObjectContext;
    } else if ([[segue identifier] isEqualToString:kshowMyOptions] ){
        ShowMyOptionsViewController *showMyOptionsViewController = segue.destinationViewController;
        showMyOptionsViewController.managedObjectContext = self.managedObjectContext;
        
    }
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( [identifier isEqualToString:kRItoAddOption] ) {
        if ( [self.potentialDC.text isEqualToString:@""] ){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No input found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}





@end
